import React, { Fragment, PureComponent } from 'react';
import CreateForm from './CreateForm';
import { connect } from 'dva';
import {
  Button,
  Card,
  Col,
  DatePicker,
  Divider,
  Form,
  Input,
  Modal,
  Row,
  Select,
  Table,
} from 'antd';
import styles from './ParamValueTable.less';

const FormItem = Form.Item;

const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ loading, paramValue }) => ({
  ...paramValue,
  loading: loading.models.paramValue,
}))
@Form.create()
class ParamValueTable extends PureComponent {
  constructor(props) {
    super(props);
    this.moduel = 'paramValue';
  }

  state = {
    modalVisible: false,
    selectedRows: [],
    tempRecord: null,
    oprType: '',
    selectedApps: [],
    list: [],
  };

  info = (content) => {
    Modal.info({
      title: '取值脚本',
      okText: '知道了',
      content: (
        <pre>
          {content}
        </pre>
      ),
      onOk() {
      },
    });
  };

  columns = [
    {
      title: '序号',
      dataIndex: 'valueIndex',
      width:'15%',
    },
    {
      title: '值来源',
      dataIndex: 'valueSource',
      width:'15%',
      render: (text, record, index) => {
        let txt = '-';
        if (text == 1)
          txt = 'URL参数';
        else if (text == 2) {
          txt = 'HEADER';
        } else if (text == 3) {
          txt = 'FORM表单';
        } else if (text == 4) {
          txt = '请求JSON';
        }
        return txt;
      },
    },
    {
      title: '取值脚本',
      width:'50%',
      dataIndex: 'valueScript',
      render: (text) => {
        if (text && text.length > 50) {
          return <a onClick={this.info.bind(null, text)}>{text.substring(0, 50)}...</a>;
        }
        return text;
      },
    },
    {
      title: '操作',

      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          <Divider type="vertical"/>
          <a onClick={() => this.deleteRouter(record.id)}>删除</a>
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    // this.fetchList();
  }

  deleteRouter = id => {
    Modal.confirm({
      title: '删除参数取值',
      content: '确定删除该参数取值相关信息吗？',
      okText: '确认',
      cancelText: '取消',
      onOk: () => this.deleteItem(id),
    });
  };

  deleteItem = id => {
    const { dispatch } = this.props;
    dispatch({
      type: `${this.moduel}/delete`,
      payload: id,
      callback: this.fetchList,
    });
  };


  fetchList = () => {
    const { dispatch, paraRuleId } = this.props;
    dispatch({
      type: `${this.moduel}/fetch`,
      payload: { paraRuleId },
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
      oprType: 'add',
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      modalVisible: !!flag,
      tempRecord: !!flag ? record : null,
      oprType: 'update',
    });
  };

  saveRoute = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: this.moduel + (fields.id ? '/update' : '/add'),
      payload: fields,
      callback: this.fetchList,
    });
    this.handleModalVisible();
  };

  render() {
    const { loading, dispatch, paraRuleId, list } = this.props;

    const {
      modalVisible,
      oprType,
      tempRecord,
    } = this.state;

    const parentMethods = {
      saveRoute: this.saveRoute,
      handleModalVisible: this.handleModalVisible,
    };

    const featureMethods = {
      handleFeatureModalVisible: this.handleFeatureModalVisible,
    };

    return (
      <div>
        <Table
          loading={loading}
          dataSource={list && list[paraRuleId]}
          columns={this.columns}
          pagination={false}
        />

        <CreateForm
          type={oprType}
          record={tempRecord}
          {...parentMethods}
          modalVisible={modalVisible}
        />
      </div>
    );
  }
}

export default ParamValueTable;
