import React, { Fragment, PureComponent } from 'react';
import { connect } from 'dva';
import {
  Button,
  Card,
  Col,
  DatePicker,
  Divider,
  Form,
  Input,
  Modal,
  Row,
  Select,
  Table,
} from 'antd';
import styles from './ParamValueTable.less';

const FormItem = Form.Item;
const { Option } = Select;

//创建路由基础信息
const CreateForm = Form.create()(props => {
  const { modalVisible, form, saveRoute, handleModalVisible, type, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      if (type == 'update') fieldsValue = { ...record, ...fieldsValue };

      saveRoute({
        ...fieldsValue,
      });
    });
  };

  const formLayout = {
    labelCol: { span: 5 },
    wrapperCol: { span: 15 },
    layout: 'horizontal',
  };


  return (
    <Modal
      destroyOnClose
      title={type == 'add' ? '添加参数取值' : '修改参数取值'}
      visible={modalVisible}
      width={600}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem {...formLayout}>
        {form.getFieldDecorator('id', {
          initialValue: record && record.id,
        })(<Input type="hidden" />)}
      </FormItem>
      <FormItem style={{display:'none'}} {...formLayout} label="参数规则ID:">
        {form.getFieldDecorator('paraRuleId', {
          initialValue: record && record.paraRuleId,
          rules: [{ required: true, message: '请输入参数规则ID！' }],
        })(<Input readOnly placeholder="请输入参数规则ID" />)}
      </FormItem>
      <FormItem {...formLayout} label="序号:">
        {form.getFieldDecorator('valueIndex', {
          initialValue: record && record.valueIndex,
          rules: [{ required: true, message: '请输入序号！' }],
        })(<Input type="number" placeholder="请输入序号" />)}
      </FormItem>
      <FormItem {...formLayout} label="值来源:">
        {form.getFieldDecorator('valueSource', {
          initialValue: record && `${record.valueSource}`,
          rules: [{ required: true, message: '请选择来源！' }],
        })(
          <Select placeholder="选择来源" style={{ width: '100%' }}>
            <Option key="1">URL参数</Option>
            <Option key="2">HEADER</Option>
            <Option key="3">FORM表单</Option>
            <Option key="4">请求JSON</Option>
          </Select>,
        )}
      </FormItem>
      <FormItem {...formLayout} label="取值脚本:">
        {form.getFieldDecorator('valueScript', {
          initialValue: record && record.valueScript,
          rules: [{ required: true, message: '请输入取值脚本！' }],
        })(<Input.TextArea  rows={4} placeholder="请输入取值脚本" />)}
      </FormItem>
    </Modal>
  );
});


export default CreateForm;
