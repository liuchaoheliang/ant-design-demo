
function DemoLayout(props) {
  return (
    <div>
      {props.children}
    </div>
  );
}

export default DemoLayout;
