import styles from './index.css';
import Link from 'umi/link';
import { formatMessage } from 'umi-plugin-locale';
export default function() {
  return (
    <div className={styles.normal}>
      <div className={styles.welcome} />
      <ul className={styles.list}>
        <li>To get started, edit <code>src/pages/index.js</code> and save to reload.</li>
        <li>
          <a href="https://umijs.org/guide/getting-started.html">
            {formatMessage({ id: 'index.start' })}
          </a>

        </li>
        <li><Link to="/demo">demo test</Link></li>
        <li><Link to="/demo/demo1">demo test1</Link></li>
        <li><Link to="/User/Login">User Login</Link></li>
        <li><Link to="/User/Register">User Register</Link></li>
        <li><Link to="/User/RegisterResult">User RegisterResult</Link></li>
      </ul>
    </div>
  );
}
