import { deletePara, createPara, updatePara, queryParaPage } from '@/services/api';
import { message } from 'antd';

export default {
  namespace: 'param',

  state: {
    list: [],
    pagination: {},
  },

  effects: {
    *fetch({ payload }, { call, put }) {
       const response = yield call(queryParaPage, payload);
      //const  response = {"success":true,"status":200,"msg":null,"data":{"content":[{"id":12,"paraName":"/test334","status":1,"paraDesc":"/test334","paraCode":"msjrapi","effectAt":"2020-01-15 16:55:58","status":1,"createdAt":"2020-01-13 16:56:21","createdBy":null,"updatedAt":"2020-01-13 18:48:26","updatedBy":null},{"id":19,"paraName":"/test1234","status":1,"paraDesc":"/test1234","paraCode":"tjms-view","effectAt":"2020-01-16 16:55:58","status":1,"createdAt":"2020-01-13 18:12:08","createdBy":null,"updatedAt":"2020-01-13 18:28:06","updatedBy":null},{"id":18,"paraName":"/asdqwe/adsad","status":0,"paraDesc":"/asdqeqw","paraCode":"asrapi","effectAt":"2020-01-29 17:42:00","status":1,"createdAt":"2020-01-13 17:42:27","createdBy":null,"updatedAt":"2020-01-13 17:42:27","updatedBy":null},{"id":9,"paraName":"/api19","status":0,"paraDesc":"/api19","paraCode":"demo-executor","effectAt":"2020-01-17 14:35:01","status":1,"createdAt":"2020-01-13 14:35:24","createdBy":null,"updatedAt":"2020-01-13 14:35:24","updatedBy":null},{"id":11,"paraName":"/api178","status":1,"paraDesc":"/api178","paraCode":"demo-executor","effectAt":"2020-01-17 14:40:38","status":1,"createdAt":"2020-01-13 14:41:01","createdBy":null,"updatedAt":"2020-01-13 14:41:01","updatedBy":null},{"id":13,"paraName":"/api1756","status":1,"paraDesc":"/api1756","paraCode":"msjrapi","effectAt":"2020-01-16 17:06:16","status":1,"createdAt":"2020-01-13 17:06:40","createdBy":null,"updatedAt":"2020-01-13 17:06:40","updatedBy":null},{"id":16,"paraName":"/api1755","status":1,"paraDesc":"/api178122","paraCode":"vopen","effectAt":"2020-01-28 17:24:24","status":1,"createdAt":"2020-01-13 17:24:46","createdBy":null,"updatedAt":"2020-01-13 17:24:46","updatedBy":null},{"id":20,"paraName":"/api175222","status":1,"paraDesc":"/api175222","paraCode":"vopen","effectAt":"2020-01-15 18:47:34","status":1,"createdAt":"2020-01-13 18:47:55","createdBy":null,"updatedAt":"2020-01-13 18:47:55","updatedBy":null},{"id":10,"paraName":"/api175","status":0,"paraDesc":"/api175","paraCode":"ngls","effectAt":"2020-01-16 14:39:46","status":1,"createdAt":"2020-01-13 14:40:08","createdBy":null,"updatedAt":"2020-01-13 14:40:08","updatedBy":null},{"id":8,"paraName":"/api17","status":1,"paraDesc":"/api17","paraCode":"msjrapi","effectAt":"2020-01-23 14:31:29","status":1,"createdAt":"2020-01-13 14:31:52","createdBy":null,"updatedAt":"2020-01-13 14:31:52","updatedBy":null}],"pageable":{"sort":{"sorted":true,"unsorted":false,"empty":false},"pageNumber":0,"pageSize":10,"offset":0,"paged":true,"unpaged":false},"totalElements":16,"last":false,"totalPages":2,"first":true,"sort":{"sorted":true,"unsorted":false,"empty":false},"numberOfElements":10,"size":10,"number":0,"empty":false}};

      yield put({
        type: 'save',
        payload: {
          list: response.data.content,
          pagination: {
            total: response.data.totalElements,
            pageSize: response.data.size,
            current: response.data.number + 1,
          },
        },
      });
    },

    *add({ payload, callback }, { call, put }) {
      const response = yield call(createPara, payload);
      if (response) {
        message.success('添加成功');
        if (callback) callback();
      }
    },

    *delete({ payload, callback }, { call, put }) {
      const response = yield call(deletePara, payload);
      if (response) {
        message.success('删除成功');
        if (callback) callback();
      }
    },

    *update({ payload, callback }, { call, put }) {
      const response = yield call(updatePara, payload);
      if (response) {
        message.success('更新成功');
        if (callback) callback();
      }
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};
