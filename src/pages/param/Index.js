import React, { Fragment, PureComponent } from 'react';
import moment from 'moment';
import { connect } from 'dva';
import {
  Button,
  Card,
  Col,
  DatePicker,
  Divider,
  Form,
  Input,
  Modal,
  Row,
  InputNumber,
  Select,
  Table,
} from 'antd';
import styles from './Index.less';

const FormItem = Form.Item;
const { Option } = Select;

const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');
const statusMap = ['default', 'processing', 'success', 'error'];

const dateFormat = 'YYYY-MM-DD HH:mm:ss';

//创建参数基础信息
const CreateForm = Form.create()(props => {
  const { modalVisible, form, saveRoute, handleModalVisible, type, appCodes, record } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      if (type == 'update') fieldsValue = { ...record, ...fieldsValue };

      saveRoute({
        ...fieldsValue,
      });
    });
  };

  const formLayout = {
    labelCol: { span: 5 },
    wrapperCol: { span: 15 },
    layout: 'horizontal',
  };

  return (
    <Modal
      destroyOnClose
      title={type == 'add' ? '添加参数' : '修改参数'}
      visible={modalVisible}
      width={600}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem {...formLayout}>
        {form.getFieldDecorator('id', {
          initialValue: record && record.id,
        })(<Input type="hidden" />)}
      </FormItem>
      <FormItem {...formLayout} label="参数编码:">
        {form.getFieldDecorator('paraCode', {
          initialValue: record && record.paraCode,
          rules: [{ required: true, message: '请输入参数编码！' }],
        })(<Input placeholder="请输入参数编码" />)}
      </FormItem>
      <FormItem {...formLayout} label="参数名称:">
        {form.getFieldDecorator('paraName', {
          initialValue: record && record.paraName,
          rules: [{ required: true, message: '请输入参数名称！' }],
        })(<Input placeholder="请输入参数名称" />)}
      </FormItem>
      <FormItem {...formLayout} label="优先级:">
        {form.getFieldDecorator('paraPriority', {
          initialValue: record && record.paraPriority,
          rules: [{ required: true, message: '请输入优先级！' }],
        })(<InputNumber min={0} max={65535} style={{width:'100%'}} placeholder="请输入优先级(越小越高)" />)}
      </FormItem>
      <FormItem {...formLayout} label="状态:">
        {form.getFieldDecorator('status', {
          initialValue: (record && `${record.status}`) || '1',
          rules: [{ required: true, message: '请选择状态！' }],
        })(
          <Select placeholder="选择状态" style={{ width: '100%' }}>
            <Option key={1}>有效</Option>
            <Option key={0}>无效</Option>
          </Select>,
        )}
      </FormItem>
      <FormItem {...formLayout} label="参数描述:">
        {form.getFieldDecorator('paraDesc', {
          initialValue: record && record.paraDesc,
          // rules: [{ required: true, message: '请输入参数描述！' }],
        })(<Input placeholder="请输入参数描述" />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ loading, param }) => ({
  ...param,
  loading: loading.models.param,
}))
@Form.create()
class Index extends PureComponent {
  constructor(props) {
    super(props);
    this.moduel = 'param';
  }

  state = {
    modalVisible: false,
    selectedRows: [],
    tempRecord: null,
    oprType: '',
    selectedApps: [],
  };

  columns = [
    {
      title: '参数名称',
      dataIndex: 'paraName',
    },
    {
      title: '参数编码',
      dataIndex: 'paraCode',
    },
    {
      title: '优先级',
      dataIndex: 'paraPriority',
    },
    {
      title: '参数说明',
      dataIndex: 'paraDesc',
    },
    {
      title: '状态',
      dataIndex: 'status',
      render: (text, record, index) => {
        let txt = '-';
        if (text == 1) txt = '有效';
        else if (text == 0) {
          txt = '无效';
        }
        return txt;
      },
    },
    {
      title: '操作',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          <Divider type="vertical" />
          <a onClick={() => this.deleteRouter(record.id)}>删除</a>
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: `${this.moduel}/getAppCodes`,
    });

    dispatch({
      type: `${this.moduel}/fetch`,
    });
  }

  deleteRouter = id => {
    Modal.confirm({
      title: '删除参数',
      content: '确定删除该参数相关信息吗？',
      okText: '确认',
      cancelText: '取消',
      onOk: () => this.deleteItem(id),
    });
  };

  deleteItem = id => {
    const { dispatch } = this.props;
    dispatch({
      type: `${this.moduel}/delete`,
      payload: id,
      callback: this.handleSearch,
    });
  };

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: `${this.moduel}/fetch`,
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    dispatch({
      type: `${this.moduel}/clear`,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    if (e) e.preventDefault();

    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: `${this.moduel}/fetch`,
        payload: fieldsValue,
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
      oprType: 'add',
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      modalVisible: !!flag,
      tempRecord: !!flag ? record : null,
      oprType: 'update',
    });
  };

  saveRoute = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: this.moduel + (fields.id ? '/update' : '/add'),
      payload: fields,
      callback: this.handleSearch,
    });
    this.handleModalVisible();
  };

  //查询form
  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
      appCodes,
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="参数编码">
              {getFieldDecorator('paraCode')(<Input placeholder="参数编码" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="状态">
              {getFieldDecorator('status')(
                <Select placeholder="选择状态" style={{ width: '100%' }}>
                  <Option key={1}>有效</Option>
                  <Option key={0}>无效</Option>
                </Select>,
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button onClick={this.handleFormReset}>重置</Button>
              <Button style={{ marginLeft: 8 }} type="primary" htmlType="submit">
                查询
              </Button>

              <Button
                style={{ marginLeft: 8 }}
                icon="plus"
                type="primary"
                onClick={() => this.handleModalVisible(true)}
              >
                新建
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderForm() {
    return this.renderSimpleForm();
  }

  render() {
    const { list, pagination, loading, appCodes, dispatch } = this.props;

    const {
      selectedRows,
      modalVisible,
      oprType,
      tempRecord,
    } = this.state;

    const parentMethods = {
      saveRoute: this.saveRoute,
      handleModalVisible: this.handleModalVisible,
    };

    const featureMethods = {
      handleFeatureModalVisible: this.handleFeatureModalVisible,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderForm()}</div>
            <Table
              loading={loading}
              dataSource={list}
              columns={this.columns}
              pagination={pagination}
              onChange={this.handleStandardTableChange}
            />
          </div>
        </Card>

        <CreateForm
          appCodes={appCodes}
          type={oprType}
          record={tempRecord}
          {...parentMethods}
          modalVisible={modalVisible}
        />
      </div>
    );
  }
}

export default Index;
