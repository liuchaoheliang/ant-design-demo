import React, { Fragment, PureComponent } from 'react';
import ParamValueForm from '@/components/ParamValue/CreateForm';
import ParamValueTable from '@/components/ParamValue/ParamValueTable';
import { connect } from 'dva';
import {
  Button,
  Card,
  Col,
  DatePicker,
  Divider,
  Form,
  Input,
  Modal,
  Row,
  Select,
  Table,
} from 'antd';
import styles from './Index.less';

const FormItem = Form.Item;
const { Option } = Select;

const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');
const statusMap = ['default', 'processing', 'success', 'error'];

const dateFormat = 'YYYY-MM-DD HH:mm:ss';

//创建参数规则基础信息
const CreateForm = Form.create()(props => {
  const { modalVisible, form, saveRoute, handleModalVisible, type, appCodes, record, paramList } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      if (type == 'update') fieldsValue = { ...record, ...fieldsValue };

      saveRoute({
        ...fieldsValue,
      });
    });
  };

  const formLayout = {
    labelCol: { span: 5 },
    wrapperCol: { span: 15 },
    layout: 'horizontal',
  };

  return (
    <Modal
      destroyOnClose
      title={type == 'add' ? '添加参数规则' : '修改参数规则'}
      visible={modalVisible}
      width={600}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem {...formLayout}>
        {form.getFieldDecorator('id', {
          initialValue: record && record.id,
        })(<Input type="hidden"/>)}
      </FormItem>
      <FormItem {...formLayout} label="参数:">
        {form.getFieldDecorator('paraId', {
          initialValue: record && record.paraId,
          rules: [{ required: true, message: '选择目标参数！' }],
        })(
          <Select
            showSearch
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            placeholder="选择目标参数"
            style={{ width: '100%' }}
          >
            {paramList.map(v => (
              <Option key={v.id} value={v.id}>
                {v.paraName}
              </Option>
            ))}
          </Select>,
        )}
      </FormItem>
      <FormItem {...formLayout} label="目标应用:">
        {form.getFieldDecorator('appCode', {
          initialValue: record && record.appCode,
          // rules: [{ required: true, message: '选择目标应用！' }],
        })(
          <Select
            showSearch
            optionFilterProp="children"
            filterOption={(input, option) =>
              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            placeholder="选择目标应用"
            style={{ width: '100%' }}
          >
            {appCodes.map(v => (
              <Option key={v.appName} value={v.appName}>
                {v.appName}
              </Option>
            ))}
          </Select>,
        )}
      </FormItem>
      <FormItem {...formLayout} label="请求路径:">
        {form.getFieldDecorator('path', {
          initialValue: record && record.path,
          // rules: [{ required: true, message: '请输入请求路径！' }],
        })(<Input placeholder="请输入请求路径"/>)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ loading, paramRule }) => ({
  ...paramRule,
  loading: loading.models.paramRule,
}))
@Form.create()
class Index extends PureComponent {
  constructor(props) {
    super(props);
    this.moduel = 'paramRule';
  }

  state = {
    modalVisible: false,
    selectedRows: [],
    tempRecord: null,
    oprType: '',
    selectedApps: [],
  };

  columns = [
    {
      title: '应用编码',
      dataIndex: 'appCode',
      width:'20%',
    },
    {
      title: '请求路径',
      width:'50%',
      dataIndex: 'path',
    },
    {
      title: '操作',

      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
          <Divider type="vertical"/>
          <a onClick={() => this.deleteRouter(record.id)}>删除</a>
          <Divider type="vertical"/>
          <a onClick={() => this.handleValueModalVisible(record.id)}>创建取值</a>
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: `${this.moduel}/getAppCodes`,
    });

    dispatch({
      type: `${this.moduel}/paramList`,
    });

    dispatch({
      type: `${this.moduel}/clear`,
    });
  }

  deleteRouter = id => {
    Modal.confirm({
      title: '删除参数规则',
      content: '确定删除该参数规则相关信息吗？',
      okText: '确认',
      cancelText: '取消',
      onOk: () => this.deleteItem(id),
    });
  };

  deleteItem = id => {
    const { dispatch } = this.props;
    dispatch({
      type: `${this.moduel}/delete`,
      payload: id,
      callback: this.handleSearch,
    });
  };

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: `${this.moduel}/fetch`,
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    dispatch({
      type: `${this.moduel}/clear`,
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    if (e) e.preventDefault();

    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;

      dispatch({
        type: `${this.moduel}/fetch`,
        payload: fieldsValue,
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
      oprType: 'add',
    });
  };

  handleValueModalVisible = flag => {
    this.setState({
      valueModalVisible: !!flag,
      tempRecord: { paraRuleId: flag, valueSource: '' },
      oprType: 'add',
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      modalVisible: !!flag,
      tempRecord: !!flag ? record : null,
      oprType: 'update',
    });
  };

  saveRoute = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: this.moduel + (fields.id ? '/update' : '/add'),
      payload: fields,
      callback: this.handleSearch,
    });
    this.handleModalVisible();
  };

  saveValue = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'paramValue' + (fields.id ? '/update' : '/add'),
      payload: fields,
      callback: () => {
        this.fetchValue(fields.paraRuleId);
      },
    });
    this.handleValueModalVisible();
  };

  expandedRowRender = (record) => {
    return <ParamValueTable paraRuleId={record.id}/>;
  };

  onExpand = (expanded, record) => {
    if (expanded) {
      this.fetchValue(record.id);
    }
  };

  fetchValue = (paraRuleId) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'paramValue/fetch',
      payload: { paraRuleId },
    });
  };


  //查询form
  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
      appCodes,
      paramList,
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 6, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="参数">
              {getFieldDecorator('paraId', {
                rules: [{ required: true, message: '选择参数！' }],
              })(
                <Select
                  showSearch
                  //mode="multiple"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                  placeholder="选择参数"
                  style={{ width: '100%' }}
                >
                  {paramList.map(v => (
                    <Option key={v.id} value={v.id}>
                      {v.paraName}
                    </Option>
                  ))}
                </Select>,
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="应用编码">
              {getFieldDecorator('appCode')(
                <Select
                  showSearch
                  //mode="multiple"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                  placeholder="选择应用"
                  style={{ width: '100%' }}
                >
                  {appCodes.map(v => (
                    <Option key={v.appName} value={v.appName}>
                      {v.appName}
                    </Option>
                  ))}
                </Select>,
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="请求路径">
              {getFieldDecorator('path')(<Input placeholder="请求路径"/>)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button onClick={this.handleFormReset}>重置</Button>
              <Button style={{ marginLeft: 8 }} type="primary" htmlType="submit">
                查询
              </Button>

              <Button
                style={{ marginLeft: 8 }}
                icon="plus"
                type="primary"
                onClick={() => this.handleModalVisible(true)}
              >
                新建
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderForm() {
    return this.renderSimpleForm();
  }

  render() {
    const { list, pagination, loading, appCodes, dispatch, paramList } = this.props;

    const {
      modalVisible,
      valueModalVisible,
      oprType,
      tempRecord,
    } = this.state;

    const parentMethods = {
      saveRoute: this.saveRoute,
      handleModalVisible: this.handleModalVisible,
    };

    const parentValueMethods = {
      saveRoute: this.saveValue,
      handleModalVisible: this.handleValueModalVisible,
    };

    return (
      <div>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderForm()}</div>
            <Table
              loading={loading}
              dataSource={list}
              columns={this.columns}
              pagination={pagination}
              expandedRowRender={this.expandedRowRender}
              onExpand={this.onExpand}
              onChange={this.handleStandardTableChange}
            />
          </div>
        </Card>

        <CreateForm
          appCodes={appCodes}
          paramList={paramList}
          type={oprType}
          record={tempRecord}
          {...parentMethods}
          modalVisible={modalVisible}
        />

        <ParamValueForm
          appCodes={appCodes}
          paramList={paramList}
          type={oprType}
          record={tempRecord}
          {...parentValueMethods}
          modalVisible={valueModalVisible}
        />
      </div>
    );
  }
}

export default Index;
