import {
  deleteParaRule,
  createParaRule,
  updateParaRule,
  getAppCodes,
  queryParaRulePage,
  queryParaPage,
} from '@/services/api';
import { message } from 'antd';

export default {
  namespace: 'paramRule',

  state: {
    list: [],
    pagination: {},
    appCodes: [],
    paramList: [],
  },

  effects: {
    //获取appCodes
    *getAppCodes({ payload }, { call, put }) {
      const response = yield call(getAppCodes, payload);
      yield put({
        type: 'save',
        payload: { appCodes: response.data },
      });
    },

    *paramList({ payload }, { call, put }) {
      const { data } = yield call(queryParaPage, {pageSize: 1000});
      yield put({
        type: 'save',
        payload: {
          paramList: data.content,
        },
      });
    },

    *fetch({ payload }, { call, put }) {
      const response = yield call(queryParaRulePage, payload);
      const { data } = yield call(queryParaPage, {pageSize: 1000});

      yield put({
        type: 'save',
        payload: {
          list: response.data.content,
          paramList: data.content,
          pagination: {
            total: response.data.totalElements,
            pageSize: response.data.size,
            current: response.data.number + 1,
          },
        },
      });
    },

    *add({ payload, callback }, { call, put }) {
      const response = yield call(createParaRule, payload);
      if (response) {
        message.success('添加成功');
        if (callback) callback();
      }
    },

    *delete({ payload, callback }, { call, put }) {
      const response = yield call(deleteParaRule, payload);
      if (response) {
        message.success('删除成功');
        if (callback) callback();
      }
    },

    *update({ payload, callback }, { call, put }) {
      const response = yield call(updateParaRule, payload);
      if (response) {
        message.success('更新成功');
        if (callback) callback();
      }
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    clear(state) {
      return {
        ...state,
        list:[]
      };
    },
  },
};
