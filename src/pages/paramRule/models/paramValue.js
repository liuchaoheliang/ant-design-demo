import {
  deleteParaValue,
  createParaValue,
  updateParaValue,
  queryParaValueList,
} from '@/services/api';
import { message } from 'antd';

export default {
  namespace: 'paramValue',

  state: {
    list: {},
    pagination: {},
  },

  effects: {
    * fetch({ payload: { paraRuleId } }, { call, put, select }) {
      const response = yield call(queryParaValueList, { paraRuleId, pageSize: 1000 });
      const list = yield select(state => state.paramValue.list);
      list[paraRuleId] = response.data;
      yield put({
        type: 'save',
        payload: {
          list
        },
      });
    },

    * add({ payload, callback }, { call, put }) {
      const response = yield call(createParaValue, payload);
      if (response) {
        message.success('添加成功');
        if (callback) callback();
      }
    },

    * delete({ payload, callback }, { call, put }) {
      const response = yield call(deleteParaValue, payload);
      if (response) {
        message.success('删除成功');
        if (callback) callback();
      }
    },

    * update({ payload, callback }, { call, put }) {
      const response = yield call(updateParaValue, payload);
      if (response) {
        message.success('更新成功');
        if (callback) callback();
      }
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    clear(state) {
      return {
        ...state,
        list: [],
      };
    },
  },
};
