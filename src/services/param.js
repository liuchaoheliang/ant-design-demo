import { stringify } from 'qs';
import request from '@/utils/request';

// 根据环境动态添加api前缀
/* eslint-disable */
const suffix = IS_PRO ? '' : '/api';

//  应用codes
export async function getAppCodes() {
  return request(`${suffix}/app/list`);
}

//===================================================================
// 根据ID获取参数
export async function getPara(id) {
  return request(`${suffix}/para/getPara?id=${id}`);
}

//创建参数
export async function createPara(params) {
  return request(`${suffix}/para/createPara`, {
    method: 'POST',
    body: params,
  });
}

//修改参数
export async function updatePara(params) {
  return request(`${suffix}/para/updatePara`, {
    method: 'POST',
    body: params,
  });
}

//删除参数
export async function deletePara(id) {
  return request(`${suffix}/para/deletePara?id=${id}`, {
    method: 'POST',
    // body: { routeId: id },
  });
}

//分页查询参数
export async function queryParaPage(params) {
  return request(`${suffix}/para/page?${stringify(params)}`);
}

//===================================================================
//获取参数规则
export async function getParaRule(id) {
  return request(`${suffix}/paraRule/getParaRule?id=${id}`);
}

//创建参数规则
export async function createParaRule(params) {
  return request(`${suffix}/paraRule/createParaRule`, {
    method: 'POST',
    body: params,
  });
}

//修改参数规则
export async function updateParaRule(params) {
  return request(`${suffix}/paraRule/updateParaRule`, {
    method: 'POST',
    body: params,
  });
}

//删除参数规则
export async function deleteParaRule(id) {
  return request(`${suffix}/paraRule/deleteParaRule?id=${id}`, {
    method: 'POST',
    // body: { routeId: id },
  });
}

//分页查询参数规则
export async function queryParaRulePage(params) {
  return request(`${suffix}/paraRule/page?${stringify(params)}`);
}

//===================================================================
//获取参数取值
export async function getParaValue(id) {
  return request(`${suffix}/paraValue/getParaValue?id=${id}`);
}

//创建参数取值
export async function createParaValue(params) {
  return request(`${suffix}/paraValue/createParaValue`, {
    method: 'POST',
    body: params,
  });
}

//修改参数取值
export async function updateParaValue(params) {
  return request(`${suffix}/paraValue/updateParaValue`, {
    method: 'POST',
    body: params,
  });
}

//删除参数取值
export async function deleteParaValue(id) {
  return request(`${suffix}/paraValue/deleteParaValue?id=${id}`, {
    method: 'POST',
    // body: { routeId: id },
  });
}

//条件查询参数取值
export async function queryParaValueList(params) {
  return request(`${suffix}/paraValue/listParaValue?${stringify(params)}`);
}
